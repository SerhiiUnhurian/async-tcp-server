#include <iostream>
#include <memory>
#include <boost/asio.hpp>

#include "Service.h"
#include "Acceptor.h"

Acceptor::Acceptor(boost::asio::io_context& ioc, unsigned short port_num)
        : _ioc(ioc), _acceptor(_ioc, boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::any(), port_num)) { }

void Acceptor::start() {
    _acceptor.listen();
    initAccept();
}

void Acceptor::initAccept() {
    std::shared_ptr<boost::asio::ip::tcp::socket> socket = std::make_shared<boost::asio::ip::tcp::socket>(_ioc);
    _acceptor.async_accept(*socket, [this, socket](const boost::system::error_code& ec) {
        onAccept(ec, socket);
    });
}

void Acceptor::onAccept(const boost::system::error_code& ec, std::shared_ptr<boost::asio::ip::tcp::socket> socket) {
    if (ec.value() != 0) {
        std::cout << "Error occurred! Error code = " << ec.value()
                  << ". Message: " << ec.message();

        return;
    }

    (new Service(socket, _fileGuard))->startHandleClient();

    if (!_isStopped.load()) {
        initAccept();
    } else {
        _acceptor.close();
    }
}

void Acceptor::stop() {
    _isStopped.store(true);
}

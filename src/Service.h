#ifndef ASYNCTCPSERVER_SERVICE_H
#define ASYNCTCPSERVER_SERVICE_H

#include <fstream>
#include <boost/asio.hpp>

class Service {
private:
    std::shared_ptr<boost::asio::ip::tcp::socket> _socket;
    boost::asio::streambuf _requestBuff;
    std::string _response;
    std::mutex& _fileGuard;
    std::fstream _fileStream;

    std::string addData(std::istream& requestStream);
    std::string getData(std::istream& requestStream);
    std::string setData(std::istream& requestStream);
    std::string deleteData(std::istream& requestStream);

public:
    Service(std::shared_ptr<boost::asio::ip::tcp::socket> socket, std::mutex& fileGuard);
    void startHandleClient();
    void onRequestReceived(const boost::system::error_code& ec, std::size_t bytes_transferred);
    void onResponseSent(const boost::system::error_code& ec, std::size_t bytes_transferred);
    std::string processRequest();
    void onFinish();
};

#endif //ASYNCTCPSERVER_SERVICE_H

#include <iostream>
#include <filesystem>
#include <boost/asio.hpp>
#include <nlohmann/json.hpp>

#include "Service.h"

using json = nlohmann::json;
const std::filesystem::path FILE_PATH = std::filesystem::current_path().parent_path() / "key-value.json";


Service::Service(std::shared_ptr<boost::asio::ip::tcp::socket> socket, std::mutex& fileGuard) : _socket(socket), _fileGuard(fileGuard) { }

std::string Service::addData(std::istream& requestStream) {
    json data = json::object({});
    std::string key, value;

    requestStream  >> key >> value;

    if (_fileStream.peek() != EOF) {
        _fileStream >> data;
    }

    data[key] = value;

    _fileStream.seekg(0);
    _fileStream << data.dump(4);

    return "OK";
}

std::string Service::getData(std::istream& requestStream) {
    json data = json::object({});
    std::string key;

    requestStream  >> key;

    if (_fileStream.peek() == EOF) {
        return "Not found.";
    }

    _fileStream >> data;

    if (data.find(key) != data.end()) {
        return data[key];
    } else {
        return "Not found.";
    }
}

std::string Service::setData(std::istream& requestStream) {
    json data = json::object({});
    std::string key, value;

    requestStream >> key >> value;

    if (_fileStream.peek() == EOF) {
        return "Not found.";
    }

    _fileStream >> data;

    if (data.find(key) == data.end()) {
        return "Not found.";
    }

    data[key] = value;

    _fileStream.seekg(0);
    _fileStream << data.dump(4);

    return "OK";
}

std::string Service::deleteData(std::istream& requestStream) {
    namespace fs = std::filesystem;
    json data = json::object({});
    std::string key;

    requestStream  >> key;

    if (_fileStream.peek() == EOF) {
        return "Not found.";
    }

    _fileStream >> data;

    if (data.find(key) == data.end()) {
        return "Not found.";
    }

    const std::string tempFileName = std::tmpnam(nullptr);
    std::ofstream tempFile(tempFileName);
    if (!tempFile) {
        std::cout << "Failed to open temporary file." << std::endl;
        return "Server internal error.";
    }

    data.erase(key);

    tempFile << data.dump(4);

    fs::rename(tempFileName, FILE_PATH);

    return "OK";
}

void Service::startHandleClient() {
    boost::asio::async_read_until(*_socket, _requestBuff, '\n', [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
        onRequestReceived(ec, bytes_transferred);
    });
}

void Service::onRequestReceived(const boost::system::error_code& ec, std::size_t bytes_transferred) {
    if (ec.value() != 0) {
        std::cout << "Error occurred! Error code = " << ec.value()
                  << ". Message: " << ec.message();

        onFinish();
        return;
    }

    _response = processRequest() + '\n';

    boost::asio::async_write(*_socket, boost::asio::buffer(_response),
                             [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
                                 onResponseSent(ec, bytes_transferred);
                             });
}

void Service::onResponseSent(const boost::system::error_code& ec, std::size_t bytes_transferred) {
    if (ec.value() != 0) {
        std::cout << "Error occurred! Error code = " << ec.value()
                  << ". Message: " << ec.message();
    }

    onFinish();
}

std::string Service::processRequest() {
    std::string command, response;

    std::istream requestStream(&_requestBuff);
    requestStream >> command;

    std::transform(command.begin(), command.end(), command.begin(),
                   [](unsigned char c) {
                       return std::tolower(c);
                   });

    std::unique_lock<std::mutex> fileLock(_fileGuard);

    _fileStream.open(FILE_PATH);
    if (!_fileStream.is_open()) {
        std::ofstream out(FILE_PATH);
        _fileStream.open(FILE_PATH);
    }

    if (command == "add") {
        response = addData(requestStream);
    } else if (command == "get") {
        response = getData(requestStream);
    } else if (command == "set") {
        response = setData(requestStream);
    } else if (command == "delete") {
        response = deleteData(requestStream);
    } else {
        response = "Invalid request";
    }

    _fileStream.close();

    return response;
}

void Service::onFinish() {
    delete this;
}


/*
 * "add <tag_name> <tag_value>"
 * "get <tag_name>"
 * "set <tag_name> <tag_value>"
 * "delete <tag_name>"
 *
 * */

#include <iostream>
#include <boost/asio.hpp>
#include <memory>
#include <nlohmann/json.hpp>

#include "AsyncTCPServer.h"

AsyncTCPServer::AsyncTCPServer() {
        _work = std::make_unique<boost::asio::io_context::work>(_ioc);
    }

std::string AsyncTCPServer::sendRequestToItself(std::string request) {
    std::string response;
    boost::asio::streambuf responseBuff;
    boost::system::error_code ec;
    unsigned short port_num = 3333;

    boost::asio::ip::tcp::socket socket(_ioc, boost::asio::ip::tcp::v4());
    socket.connect(boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::any(), port_num));

    request += '\n';
    boost::asio::write(socket, boost::asio::buffer(request), ec);

    if (ec.value() != 0) {
        return "Error occurred while sending request!";
    } else {
        boost::asio::read_until(socket, responseBuff, '\n', ec);
    }

    if (ec.value() != 0) {
        return "Error occurred while reading request!";
    }

    std::istream istrm(&responseBuff);
    std::getline(istrm, response);

    socket.close();

    return response;
}

void AsyncTCPServer::start(unsigned short port_num, unsigned int threads_num) {
    assert(threads_num > 0);

    _acc = std::make_unique<Acceptor>(_ioc, port_num);
    _acc->start();

    for (unsigned int i = 0; i < threads_num; i++) {
        _thread_pool.create_thread([this](){
            _ioc.run();
        });
    }
}

void AsyncTCPServer::stop() {
    _acc->stop();
    _ioc.stop();

    _thread_pool.join_all();
}

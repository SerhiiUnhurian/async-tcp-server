#ifndef ASYNCTCPSERVER_ACCEPTOR_H
#define ASYNCTCPSERVER_ACCEPTOR_H

#include <iostream>
#include <memory>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

class Acceptor {
private:
    boost::asio::io_context& _ioc;
    boost::asio::ip::tcp::acceptor _acceptor;
    std::atomic<bool> _isStopped {false};
    std::mutex _fileGuard;

public:
    Acceptor(boost::asio::io_context& ioc, unsigned short port_num);
    void start();
    void initAccept();
    void onAccept(const boost::system::error_code& ec, std::shared_ptr<boost::asio::ip::tcp::socket> socket);
    void stop();
};


#endif //ASYNCTCPSERVER_ACCEPTOR_H

#ifndef ASYNCTCPSERVER_ASYNCTCPSERVER_H
#define ASYNCTCPSERVER_ASYNCTCPSERVER_H

#include "Acceptor.h"

class AsyncTCPServer {
private:
    boost::asio::io_context _ioc;
    std::unique_ptr<boost::asio::io_context::work> _work;
    boost::thread_group _thread_pool;
    std::unique_ptr<Acceptor> _acc;

public:
    AsyncTCPServer();
    std::string sendRequestToItself(std::string request);
    void start(unsigned short port_num, unsigned int threads_num);
    void stop();

};


#endif //ASYNCTCPSERVER_ASYNCTCPSERVER_H

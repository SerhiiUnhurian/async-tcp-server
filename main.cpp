#include <iostream>
#include <thread>

#include "src/AsyncTCPServer.h"

const unsigned int DEFAULT_THREADS_NUM = 2;

int main() {
    unsigned short port_num = 3333;

    try {
        AsyncTCPServer server;

        unsigned int threads_num = std::thread::hardware_concurrency() * 2;

        if (threads_num == 0) {
            threads_num = DEFAULT_THREADS_NUM;
        }

        server.start(port_num, threads_num);

        std::cout << server.sendRequestToItself("add name Sergey") << std::endl;
        std::cout << server.sendRequestToItself("add surname Ung") << std::endl;
        std::cout << server.sendRequestToItself("set surname Unguryan") << std::endl;
        std::cout << server.sendRequestToItself("get surname") << std::endl;
        std::cout << server.sendRequestToItself("delete surname") << std::endl;
        std::cout << server.sendRequestToItself("delete name") << std::endl;
        std::cout << server.sendRequestToItself("delete name") << std::endl;

        std::this_thread::sleep_for(std::chrono::seconds(60));

        server.stop();
    }
    catch (boost::system::system_error& e) {
        std::cout  << "Error occurred! Error code = "
                   <<e.code() << ". Message: "
                   <<e.what();
    }

    return 0;
}
